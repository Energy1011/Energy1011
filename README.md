<div align='center' style="display: flex; flex-wrap: wrap; justify-content: center; align-items: flex-start; column-gap: 20px;">

[![Welcome](https://readme-typing-svg.demolab.com?font=Fira+Code&weight=600&duration=1000&pause=1000&color=2EB82E&center=true&width=435&lines=Hi+there+%F0%9F%91%8B+%F0%9F%98%83+;I'm+Full-Stack+Web+Developer+%F0%9F%96%A5%EF%B8%8F;10%2B+Years+of+Experience;I+%E2%9D%A4%EF%B8%8F+FOSS+;Find+my+repositories+in+Gitlab;Visit+my+blog+MonsterPenguin+%F0%9F%90%A7)](https://gitlab.com/Energy1011)

I'm a Full-Stack Web Developer from Latam with 10+ years of experience, passionate about Programming, GNU/Linux, Cybersecurity and more...

</div>

-----


<div align='center' style="display: flex; flex-wrap: wrap; justify-content: center; align-items: flex-start; column-gap: 20px;">

### My most used technologies
![](https://img.shields.io/badge/Javascript%20ES6%2B-F7DF1E?style=for-the-badge&logo=javascript&logoColor=black)
![](https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white)
![](https://img.shields.io/badge/CSS3-1572B6?style=for-the-badge&logo=css3&logoColor=white)

![](https://img.shields.io/badge/GNU%20Linux%20%F0%9F%92%9B-FCC624?style=for-the-badge&logo=linux&color=black)
![](https://img.shields.io/badge/Bash-4EAA25?style=for-the-badge&logo=gnubash&logoColor=white)
![](https://img.shields.io/badge/PHP-777BB4?style=for-the-badge&logo=php&logoColor=white)
![](https://img.shields.io/badge/Laravel-FF2D20?style=for-the-badge&logo=laravel&color=red&logoColor=white)
![](https://img.shields.io/badge/codeigniter-313131?style=for-the-badge&logo=codeigniter&color=C9340A&logoColor=white)



![](https://img.shields.io/badge/VueJS-4FC08D?style=for-the-badge&logo=vue.js&logoColor=white)
![](https://img.shields.io/badge/Express.js-404D59?style=for-the-badge)
![](https://img.shields.io/badge/Node.js-43853D?style=for-the-badge&logo=node.js&logoColor=white)
![](https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=white)
![](https://img.shields.io/badge/Docker-2496ED?style=for-the-badge&logo=docker&logoColor=white)
![](https://img.shields.io/badge/K8s-326CE5?style=for-the-badge&logo=kubernetes&logoColor=white)

</div>



<div align='center' style="display: flex; flex-wrap: wrap; justify-content: center; align-items: flex-start; column-gap: 20px;">

### Databases knownlage

![](https://img.shields.io/badge/MariaDB-4EA94B?style=for-the-badge&logo=mariadb&logoColor=white&color=blue)
![](https://img.shields.io/badge/PostgreSQL-404D59?style=for-the-badge&logo=postgresql&logoColor=white)
![](https://img.shields.io/badge/MongoDB-4EA94B?style=for-the-badge&logo=mongodb&logoColor=white)

*I enjoy full-stack development focus on the Backend and SysAdmin tasks*

</div>

-----


<div align='center' style="display: flex; flex-wrap: wrap; justify-content: center; align-items: flex-start; column-gap: 20px;">

### Watch my last publicated videos 

*At my free time, I like to share recorded videos and share knowledge, with people around the globe*

*My last videos have subtitles in* __EN__ 🇺🇸/ __FR__ 🇫🇷/ __ES__ 🇪🇸

</div>



<div align='center' style="display: flex; flex-wrap: wrap; justify-content: center; align-items: flex-start; column-gap: 20px;">

[![My Youtube Channel](https://img.shields.io/badge/Click%20Here%20to%20Subscribe-Youtube-6364FF?style=for-the-badge&logo=youtube&color=red)](https://www.youtube.com/@monsterpenguin?sub_confirmation=1)


➡️ [__Run programs and hide images with C language__](https://www.youtube.com/watch?v=C5X5puNpCHM)

➡️ [__Telegram the long-waited feature? (Topics in Groups)__](https://www.youtube.com/watch?v=WqIABTFOlh8)


</div>

-----


<div align='center' style="display: flex; flex-wrap: wrap; justify-content: center; align-items: flex-start; column-gap: 20px;">

### Want to contrib in (FOSS: Free Software/Open-Source Projects) ? 

*I invite you to contrib in my FOSS projects*

| Project Name| Description     | Stats |
|--------------|-----------|------------|
| [__Pscaff__](https://gitlab.com/Energy1011/pscaff)  |*Project Scaffolding for any programming languaje*       |[![Pscaff](https://img.shields.io/npm/dt/pscaff?color=red&label=PScaff%20Downloads&logo=npm&logoColor=a&style=flat-square)](https://www.npmjs.com/package/pscaff)|
|[__Marvin-bot__ ](https://gitlab.com/Energy1011/telegram-marvin-bot) |*Multiporpuse Telegram chat bot*|[![Marvin-bot](https://img.shields.io/docker/pulls/energy10/marvin-bot?label=DockerHub%20Pulls&style=flat-square)](https://hub.docker.com/repository/docker/energy10/marvin-bot/general)|


### Join to my communities and Quora spaces

*Join and share and learn about programming, free software and open-source:*

| Space Name | Stats | Language
|--------------|-----------|---|
| [__Informática y Programación__](https://informaticayprogramacion.quora.com/)  | [![Quora](https://img.shields.io/badge/Quora%20%2B6,5k%20followers-B92B27?style=for-the-badge&logo=quora)](https://informaticayprogramacion.quora.com/)     |🇪🇸
| [__Software Libre__](https://softwarelibre1.quora.com/)  | [![Quora](https://img.shields.io/badge/Quora%20%2B900%20followers-B92B27?style=for-the-badge&logo=quora)](https://softwarelibre1.quora.com/)  |🇪🇸
| [__MonsterPenguin Group__](https://t.me/monsterpenguin)  | [![Telegram](https://img.shields.io/badge/telegram-26A5E4?style=for-the-badge&logo=telegram)](https://t.me/monsterpenguin)  |🇪🇸 🇺🇸

</div>

-----


<div align='center' style="display: flex; flex-wrap: wrap; justify-content: center; align-items: flex-start; column-gap: 20px;">

### Connect with me

🗣️ *I am a native spanish speaker with intermediate english (B1-B2)*
<br>

[![Contact](https://readme-typing-svg.demolab.com?font=Fira+Code&weight=600&duration=1000&pause=1000&color=2EB82E&center=true&width=435&lines=Hi+there+%F0%9F%91%8B+%F0%9F%98%83+;I'm+Full-Stack+Web+Developer+%F0%9F%96%A5%EF%B8%8F;10%2B+Years+of+Experience;I+%E2%9D%A4%EF%B8%8F+FOSS+;Find+my+repositories+in+Gitlab;Visit+my+blog+MonsterPenguin+%F0%9F%90%A7)](https://gitlab.com/Energy1011)

<p>
<a href="https://twitter.com/intent/follow?screen_name=Mpenguinblog"><img src="https://img.shields.io/twitter/follow/mpenguin?label=Twitter&logo=twitter&style=for-the-badge" alt="Twitter"></a> <a href="https://t.me/Energy1011"><img src="https://img.shields.io/badge/telegram-26A5E4?style=for-the-badge&logo=telegram" alt="MonsterPenguin Telegram Group"></a> <a href="https://mastodon.social/@Mpenguinblog"><img src="https://img.shields.io/badge/Mastodon-005571?style=for-the-badge&amp;logo=mastodon&amp;logoColor=white" alt="Mastodon"></a> <a href="https://energy1011.gitlab.io/monsterpenguin/"><img src="https://img.shields.io/badge/Monster%20Penguin-005571?style=for-the-badge&amp;logo=blogger&amp;logoColor=white&amp;color=blue" alt="Monster Penguin Blog"></a> 

</p>


</div>

-----

<div align='center' style="display: flex; flex-wrap: wrap; justify-content: center; align-items: flex-start; column-gap: 20px;">
![visitors-since-27-feb-2023](https://visitor-badge.laobi.icu/badge?page_id=energy.gitlab.visitor-badge&left_text=Profile%20views) <br>
</div>

